#include<algorithm>
using std::copy;

#include<iterator>
using std::ostream_iterator;

#include<iostream>
using std::showpos;

#include <cstring>
using std::atol;

#include "SimpleCompiler.h"
#include "SimpletronEmulator.h"
#include "Stack.h"

SimpleCompiler::SimpleCompiler()
{
	instructionCounter = 0;
	dataCounter = SIZETABLE - 1;

	tableEntry emptySymbol = { -1,-1,-1 };
	symbolTable.resize(SIZETABLE,emptySymbol);
	flags.resize(SIZETABLE,-1);
	
}
void SimpleCompiler::compile()
{
	ifstream simpleCode;
	simpleCode.open("simpleCode.txt", ios::in);
	if (!simpleCode)
	{
		cout << "Error : compiler doesn't open file with simple code!\n";
		return;
	}
	const int SIZESTR = 100;
	char str[SIZESTR] = "";

	SimpletronEmulator simpletronEmulator;

	while (simpleCode.peek() != EOF)
	{
		simpleCode.getline(str, SIZESTR);

		char* tokenPtr = strtok(str, " ");
		int sym = atoi(tokenPtr);

		int symLocation = searchSymbol(sym, 'L');
		if (symLocation == -1)
			createNewSymbol(sym, 'L', instructionCounter);

		tokenPtr = strtok(NULL, " ");
		if (strcmp(tokenPtr, "rem") == 0)
		{
			continue;
		}
		else if (strcmp(tokenPtr, "input") == 0)
		{

			tokenPtr = strtok(NULL, " ");
			int symbol = *(tokenPtr);

			int symLocation = searchSymbol(symbol, 'V');
			if (symLocation == -1)
			{
				createNewSymbol(symbol, 'V', dataCounter);
				symLocation = dataCounter--;
			}

			smlInstructions.push_back(10 * 100 + symLocation);
			//smlCode << "+" << (10 * 100 + symLocation) << "\n";
			instructionCounter++;

			continue;
		}
		else if (strcmp(tokenPtr, "if") == 0)
		{
			bool flag = false;
			tokenPtr = strtok(NULL, " ");
			int symbol = atoi(tokenPtr);
			if (symbol == 0 && *(tokenPtr) != '0')
			{
				symbol = *(tokenPtr);
				flag = true;
			}

			int symLocation = searchSymbol(symbol, (flag ? 'V' : 'C'));
			if (symLocation == -1)
			{
				createNewSymbol(symbol, (flag ? 'V' : 'C'), dataCounter);
				symLocation = dataCounter--;
				if (flag == false)
					simpletronEmulator.memory[symLocation] = sym;
			}

			tokenPtr = strtok(NULL, " ");

			int numberOfOperator = -1;
			if (strcmp(tokenPtr, "==") == 0)
				numberOfOperator = 0;
			else if (strcmp(tokenPtr, "!=") == 0)
				numberOfOperator = 1;
			else if (strcmp(tokenPtr, ">") == 0)
				numberOfOperator = 2;
			else if (strcmp(tokenPtr, ">=") == 0)
				numberOfOperator = 3;
			else if (strcmp(tokenPtr, "<") == 0)
				numberOfOperator = 4;
			else if (strcmp(tokenPtr, "<=") == 0)
				numberOfOperator = 5;

			flag = false;
			tokenPtr = strtok(NULL, " ");
			symbol = atoi(tokenPtr);
			if (symbol == 0 && *(tokenPtr) != '0')
			{
				symbol = *(tokenPtr);
				flag = true;
			}
			int symLocation2 = searchSymbol(symbol, (flag ? 'V' : 'C'));
			if (symLocation2 == -1)
			{
				createNewSymbol(symbol, (flag ? 'V' : 'C'), dataCounter);
				symLocation2 = dataCounter--;
				if (flag == false)
					simpletronEmulator.memory[symLocation2] = symbol;
					
			}

			tokenPtr = strtok(NULL, " ");
			tokenPtr = strtok(NULL, " ");
			symbol = atoi(tokenPtr);
			int symLocation3 = searchSymbol(symbol, 'L');

			enum operators { EQUAL, NOT_EQUAL, GREATER, GREATER_OR_EQUAL, LESS, LESS_OR_EQUAL };

			switch (numberOfOperator)
			{
			case EQUAL:
			{
				smlInstructions.push_back(20 * 100 + symLocation);
				smlInstructions.push_back(31 * 100 + symLocation2);
				//smlCode << "+" << 20 * 100 + symLocation << "\n";
				//smlCode << "+" << 31 * 100 + symLocation2 << "\n";
				instructionCounter += 2;
				if (symLocation3 == -1)
				{
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4200);
					//smlCode << "+4200\n";
				}
				else
				{
					smlInstructions.push_back(42 * 100 + symLocation3);
					//smlCode << "+" << 42 * 100 + symLocation3 << "\n";
					instructionCounter++;
				}

				break;
			}
			case NOT_EQUAL:
			{
				smlInstructions.push_back(20 * 100 + symLocation);
				smlInstructions.push_back(31 * 100 + symLocation2);
				//smlCode << "+" << 20 * 100 + symLocation << "\n";
				//smlCode << "+" << 31 * 100 + symLocation2 << "\n";
				instructionCounter += 2;
				if (symLocation3 == -1)
				{
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4100);
					//smlCode << "+4100\n";
				}
				else
				{
					smlInstructions.push_back(41 * 100 + symLocation3);
					//smlCode << "+" << 41 * 100 + symLocation3 << "\n";
					instructionCounter++;
				}
				smlInstructions.push_back(20 * 100 + symLocation2);
				smlInstructions.push_back(31 * 100 + symLocation);
				//smlCode << "+" << 20 * 100 + symLocation2 << "\n";
				//smlCode << "+" << 31 * 100 + symLocation << "\n";
				instructionCounter += 2;
				if (symLocation3 == -1)
				{
					smlInstructions.push_back(4100);
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4100);
					//smlCode << "+4100\n";
				}
				else
				{
					smlInstructions.push_back(41 * 100 + symLocation3);
					//smlCode << "+" << 41 * 100 + symLocation3 << "\n";
					instructionCounter++;
				}
				break;
			}
			case GREATER:
			{
				smlInstructions.push_back(20 * 100 + symLocation2);
				smlInstructions.push_back(31 * 100 + symLocation);
				//smlCode << "+" << 20 * 100 + symLocation2 << "\n";
				//smlCode << "+" << 31 * 100 + symLocation << "\n";
				instructionCounter += 2;
				if (symLocation3 == -1)
				{
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4100);
					//smlCode << "+4100\n";
				}
				else
				{
					smlInstructions.push_back(41 * 100 + symLocation3);
					//smlCode << "+" << 41 * 100 + symLocation3 << "\n";
					instructionCounter++;
				}
				break;
			}
			case GREATER_OR_EQUAL:
			{
				smlInstructions.push_back(20 * 100 + symLocation2);
				smlInstructions.push_back(31 * 100 + symLocation);
				//smlCode << "+" << 20 * 100 + symLocation2 << "\n";
				//smlCode << "+" << 31 * 100 + symLocation << "\n";
				instructionCounter += 2;
				if (symLocation3 == -1)
				{
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4100);
					//smlCode << "+4100\n";
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4000);
					//smlCode << "+4000\n";
				}
				else
				{
					smlInstructions.push_back(41 * 100 + symLocation3);
					smlInstructions.push_back(40 * 100 + symLocation3);
					//smlCode << "+" << 41 * 100 + symLocation3 << "\n";
					//smlCode << "+" << 40 * 100 + symLocation3 << "\n";
					instructionCounter += 2;
				}
				break;
			}
			case LESS:
			{
				smlInstructions.push_back(20 * 100 + symLocation);
				smlInstructions.push_back(31 * 100 + symLocation2);
				//smlCode << "+" << 20 * 100 + symLocation << "\n";
				//smlCode << "+" << 31 * 100 + symLocation2 << "\n";
				instructionCounter += 2;
				if (symLocation3 == -1)
				{
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4100);
					//smlCode << "+4100\n";
				}
				else
				{
					smlInstructions.push_back(41 * 100 + symLocation3);
					//smlCode << "+" << 41 * 100 + symLocation3 << "\n";
					instructionCounter++;
				}
				break;
			}
			case LESS_OR_EQUAL:
			{
				smlInstructions.push_back(20 * 100 + symLocation);
				smlInstructions.push_back(31 + 100 + symLocation2);
				//smlCode << "+" << 20 * 100 + symLocation << "\n";
				//smlCode << "+" << 31 + 100 + symLocation2 << "\n";
				instructionCounter += 2;
				if (symLocation3 == -1)
				{
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4100);
					//smlCode << "+4100\n";
					flags[instructionCounter++] = symbol;
					smlInstructions.push_back(4000);
					//smlCode << "+4000\n";
				}
				else
				{
					smlInstructions.push_back(41 * 100 + symLocation3);
					smlInstructions.push_back(40 * 100 + symLocation3);
					//smlCode << "+" << 41 * 100 + symLocation3 << "\n";
					//smlCode << "+" << 40 * 100 + symLocation3 << "\n";
					instructionCounter += 2;
				}
				break;
			}
			default:
			{
				cout << "Error : incorrect comparison operator!\n";
			}
			}
			continue;
		}
		else if (strcmp(tokenPtr, "let") == 0)
		{
			tokenPtr = strtok(NULL, " ");
			int symbol = *(tokenPtr);

			int symLocation = searchSymbol(symbol, 'V');
			if (symLocation == -1)
			{
				createNewSymbol(symbol, 'V', dataCounter);
				symLocation = dataCounter--;
			}

			tokenPtr = strtok(NULL, " ");

			vector<string> infix, postfix;

			tokenPtr = strtok(NULL, " ");
			while (tokenPtr != NULL)
			{
				bool flag = false;
				int symbol = atoi(tokenPtr);
				if (symbol == 0 && *(tokenPtr) != '0')
				{
					symbol = *(tokenPtr);
					flag = true;
				}

				int symLocation = searchSymbol(symbol, (flag ? 'V' : 'C'));
				if (symLocation == -1 && !isOperator(tokenPtr))
				{
					createNewSymbol(symbol, (flag ? 'V' : 'C'), dataCounter);
					symLocation = dataCounter--;
					if (flag == false)
						simpletronEmulator.memory[symLocation] = symbol;
				}
				infix.push_back(tokenPtr);

				tokenPtr = strtok(NULL, " ");
			}

			postfix = convertToPostfix(infix);

			int resultLocation = evaluatePostfixExpression(postfix);

			smlInstructions.push_back(20 * 100 + resultLocation);
			smlInstructions.push_back(21 * 100 + symLocation);
			//smlCode << "+" << 20 * 100 + resultLocation << "\n";
			//smlCode << "+" << 21 * 100 + symLocation << "\n";
			instructionCounter += 2;
			continue;
		}
		else if (strcmp(tokenPtr, "goto") == 0)
		{
			tokenPtr = strtok(NULL, " ");
			int symbol = atoi(tokenPtr);
			int symLocation = searchSymbol(symbol, 'L');
			if (symLocation == -1)
			{
				flags[instructionCounter] = symbol;
				smlInstructions.push_back(4000);
				//smlCode << "+4000\n";
			}
			else
				smlInstructions.push_back(40 * 100 + symLocation);
				//smlCode << "+" << 40 * 100 + symLocation << "\n";
			instructionCounter++;
			continue;
		}
		else if (strcmp(tokenPtr, "print") == 0)
		{
			tokenPtr = strtok(NULL, " ");
			int symbol = *(tokenPtr);

			int symLocation = searchSymbol(symbol, 'V');
			if (symLocation == -1)
			{
				createNewSymbol(symbol, 'V', dataCounter);
				symLocation = dataCounter--;
			}

			smlInstructions.push_back(11 * 100 + symLocation);
			//smlCode << "+" << 11 * 100 + symLocation << "\n";
			instructionCounter++;
			continue;
		}
		else if (strcmp(tokenPtr, "end") == 0)
		{
			smlInstructions.push_back(4300);
			//smlCode << "+4300\n";
			instructionCounter++;
		}
	}

	for (int i = 0; i < SIZETABLE; i++)
		if (flags[i] != -1)
		{
			int symLocation = searchSymbol(flags[i], 'L');
			smlInstructions[i] += symLocation;
		}

	if (instructionCounter - dataCounter > 1)
	{
		cout << "Error : simpletron memory overflow!\n";
		return;
	}
	ofstream smlCode("smlCode.txt", ios::out);
	if (!smlCode)
	{
		cout << "Error : compiler doesn't open file \"smlCode.txt\"!" << "\n";
		return;
	}

	smlCode << showpos;
	ostream_iterator<int> smlCode_it(smlCode, "\n");
	copy(smlInstructions.begin(), smlInstructions.end(), smlCode_it);
	smlCode.close();

	simpletronEmulator.execute();
}
int SimpleCompiler::searchSymbol(int sym, char type) const
{
	const int SIZE = symbolTable.size();
	for (int i = 0; i < SIZE; i++)
		if (symbolTable[i].symbol == sym && symbolTable[i].type == type)
			return symbolTable[i].location;
	return -1;
}
int SimpleCompiler::evaluatePostfixExpression(vector<string>& postfix)
{
	const string alphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
	Stack<int> stackInt;
	int SIZE = postfix.size();
	for (int i = 0; i < SIZE; i++)
	{
		//if postfix[i] is number or variable
		if (atol(postfix[i].c_str()) != 0
			|| (atol(postfix[i].c_str()) == 0 && postfix[i] == "0")
			|| (postfix[i].size() == 1 && alphabet.find(postfix[i]) != string::npos))
		{
			bool flag = false;
			int symbol = atoi(postfix[i].c_str());
			if (symbol == 0 && postfix[i][0] != '0')
			{
				symbol = postfix[i][0];
				flag = true;
			}

			int symLocation = searchSymbol(symbol, (flag ? 'V' : 'C'));
			stackInt.push(symLocation);
		}
		if (isOperator(postfix[i]))
		{
			int x = stackInt.top();
			stackInt.pop();
			int y = stackInt.top();
			stackInt.pop();

			smlInstructions.push_back(20 * 100 + y);
			smlInstructions.push_back(smlCodeOperator(postfix[i]) * 100 + x);
			//smlCode << "+" << 20 * 100 + y << "\n";
			//smlCode << "+" << smlCodeOperator(postfix[i]) << x << "\n";
			instructionCounter += 2;

			stackInt.push(dataCounter);
			smlInstructions.push_back(21 * 100 + dataCounter--);
			//smlCode << "+" << 21 * 100 + dataCounter-- << "\n";
			instructionCounter++;
		}
	}
	return stackInt.top();
}
int SimpleCompiler::smlCodeOperator(const string& opStr) const
{
	char op = opStr[0];
	switch (op)
	{
	case '+':
		return 30;
	case '-':
		return 31;
	case '/':
		return 32;
	case '*':
		return 33;
	}
}
vector<string> SimpleCompiler::convertToPostfix(vector<string>& infix)
{
	Stack<string> stackChar;
	vector<string> postfix;
	const string alphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
	int indexInfix = 0;
	//implementation algorithm from excercise
	stackChar.push("(");
	infix.push_back(")");
	while (!stackChar.isEmpty())
	{
		//if infix[indexInfix] is number or variable
		if (atol(infix[indexInfix].c_str()) != 0
			|| (atol(infix[indexInfix].c_str()) == 0 && infix[indexInfix] == "0")
			|| (infix[indexInfix].size() == 1 && alphabet.find(infix[indexInfix]) != string::npos))
			postfix.push_back(infix[indexInfix]);
		if (infix[indexInfix] == "(")
			stackChar.push(infix[indexInfix]);
		//if infix[indexInfix] is operator
		if (isOperator(infix[indexInfix]))
		{
			while (isOperator(stackChar.top()) && precedence(stackChar.top(), infix[indexInfix]) >= 0)
			{
				postfix.push_back(stackChar.top());
				stackChar.pop();
			}
			stackChar.push(infix[indexInfix]);//push operator in stack
		}
		if (infix[indexInfix] == ")")
		{
			while (stackChar.top() != "(")
			{
				if (isOperator(stackChar.top()))
					postfix.push_back(stackChar.top());
				stackChar.pop();
			}
			stackChar.pop();//discard '('
		}
		indexInfix++;
	}
	return postfix;
}
int SimpleCompiler::precedence(const string& operator1, const string& operator2) const
{
	vector<string> operators = { "+","-","*","/","^","%" };
	const int SIZE = 6;
	int rankOfOperators[SIZE] = { 1,1,2,2,3,2 };//order of rank responds order of operators in string "operators"

	int index1, index2;//define operator1 and operator2
	for (int i = 0; i < SIZE; i++)
	{
		if (operator1 == operators[i])
			index1 = i;
		if (operator2 == operators[i])
			index2 = i;
	}

	if (rankOfOperators[index1] < rankOfOperators[index2])
		return -1;
	else if (rankOfOperators[index1] == rankOfOperators[index2])
		return 0;
	else
		return 1;
}
bool SimpleCompiler::isOperator(const string& str) const
{
	vector<string> operators = { "+","-","*","/","^","%" };
	int SIZE = operators.size();
	for (int i = 0; i < SIZE; i++)
		if (str == operators[i])
			return true;
	return false;
}
void SimpleCompiler::createNewSymbol(const int sym,const char typ,const int loc)
{
	tableEntry newObj = { sym,typ,loc };
	symbolTable.push_back(newObj);
}