#ifndef SIMPLETRON_EMULATOR_H
#define SIMPLETRON_EMULATOR_H

#include <fstream>
using std::ifstream;
using std::ofstream;
using std::ios;

class SimpleCompiler;

class SimpletronEmulator
{
	friend SimpleCompiler;
public:
	SimpletronEmulator();
	void execute();
	void dumpMemory();
private:
	int accumulator;
	int instructionCounter;
	int instructionRegister;
	int operationCode;
	int operand;
	static const int SIZEMEMORY = 100;
	int memory[SIZEMEMORY];
	ofstream debug;
};
#endif
