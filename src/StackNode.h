#ifndef STACKNODE_H
#define STACKNODE_H

#include<iostream>
using std::cout;

template<typename STACKTYPE> class Stack;//for declaration friend(class Stack)

template<typename STACKTYPE>
class StackNode
{
	friend Stack<STACKTYPE>;//for access to nextPtr
public:
	StackNode(const STACKTYPE&);
	~StackNode();
	STACKTYPE getValue() const;
	void setValue(STACKTYPE&);
private:
	STACKTYPE value;
	StackNode<STACKTYPE>* nextPtr;
};

template <typename STACKTYPE>
StackNode<STACKTYPE>::StackNode(const STACKTYPE& v) : value(v), nextPtr(0)
{
}
template <typename STACKTYPE>
StackNode<STACKTYPE>::~StackNode()
{
}
template <typename STACKTYPE>
STACKTYPE StackNode<STACKTYPE>::getValue() const
{
	return value;
}
template <typename STACKTYPE>
void StackNode<STACKTYPE>::setValue(STACKTYPE& v)
{
	value = v;
}
#endif