#ifndef SIMPLE_COMPILER_H
#define SIMPLE_COMPILER_H

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <fstream>
using std::ofstream;
using std::ifstream;
using std::ios;

struct tableEntry
{
	int symbol;
	char type;
	int location;
};

class SimpleCompiler
{
public:
	SimpleCompiler();
	void compile();
private:
	
	int evaluatePostfixExpression(vector<string>&);
	vector<string> convertToPostfix(vector<string>&);
	int precedence(const string&, const string&) const;
	bool isOperator(const string&) const;
	int smlCodeOperator(const string&) const;
	int searchSymbol(int, char) const;
	void createNewSymbol(const int,const char,const int);

	int instructionCounter;
	int dataCounter;
	static const int SIZETABLE = 100;
	vector<tableEntry> symbolTable;
	vector<int> flags;
	vector<int> smlInstructions;
};

#endif // !SIMPLE_COMPILER_H

