#include <iostream>
using std::cin;
using std::cout;
using std::showpos;
using std::left;
using std::internal;
using std::right;
using std::noshowpos;

#include <iomanip>
using std::setw;
using std::setfill;

#include "SimpletronEmulator.h"

SimpletronEmulator::SimpletronEmulator()
{
	accumulator = 0;
	instructionCounter = 0;
	instructionRegister = 0;
	operationCode = 0;
	operand = 0;
	for (int i = 0; i < SIZEMEMORY; i++)
		memory[i] = 0;
	debug.open("debug.txt", ios::out);
}
void SimpletronEmulator::execute()
{
	ifstream smlCode("smlCode.txt", ios::in);
	if (!smlCode)
	{
		debug << "*** ERROR : emulator doesn't open file \"smlCode.txt\" ***\n";
		debug << "*** Simpletron crashs the program ***\n";
		return;
	}

	accumulator = 0;
	instructionCounter = 0;
	instructionRegister = 0;
	operationCode = 0;
	operand = 0;
	int enter = 0;

	debug << "*** Simpletron welcomes you! ***\n";
	while (smlCode >> enter)//load commands to memory
	{
		if (enter >= 9999 || enter < -9999)//checking error
		{
			while (enter >= 9999 || enter < -9999)
			{
				debug << "*** ERROR : incorrect input command ***\n"
					<< "*** Please enter number in range from -9999 to 9999 ***\n";
				smlCode >> enter;
			}
		}
		memory[instructionCounter] = enter;//load command
		instructionCounter++;
	}
	debug << "*** Loading program complete ***\n";
	debug << "*** Starting the program ***\n";

	int amountInstruction = instructionCounter;
	instructionCounter = 0;

	while (instructionCounter < amountInstruction)//starting the program
	{
		instructionRegister = memory[instructionCounter];//load from memory to register
		operationCode = instructionRegister / 100;
		operand = instructionRegister % 100;
		switch (operationCode)//choice command
		{
		case 10://enter number
		{
			cin >> memory[operand];
			instructionCounter++;
			break;
		}
		case 11://print number
		{
			cout << memory[operand] << "\n";
			instructionCounter++;
			break;
		}
		case 20://load number from memory to register
		{
			accumulator = memory[operand];
			if (accumulator > 9999 || accumulator < -9999)//checking error
			{
				debug << "*** ERROR : overflow accumulator ***\n***Simpletron crashs the program ***\n";
				dumpMemory();
				return;
			}
			instructionCounter++;
			break;
		}
		case 21://load number from register to memory
		{
			memory[operand] = accumulator;
			instructionCounter++;
			break;
		}
		case 30://add
		{
			accumulator += memory[operand];
			if (accumulator > 9999 || accumulator < -9999)//checking error
			{
				debug << "*** ERROR : overflow accumulator ***\n***Simpletron crashs the program ***\n";
				dumpMemory();
				return;
			}
			instructionCounter++;
			break;
		}
		case 31://substract
		{
			accumulator -= memory[operand];
			if (accumulator > 9999 || accumulator < -9999)//checking error
			{
				debug << "*** ERROR : overflow accumulator ***\n***Simpletron crashs the program ***\n";
				dumpMemory();
				return;
			}
			instructionCounter++;
			break;
		}
		case 32://divide
		{
			if (memory[operand] == 0)//checking error
			{
				debug << "*** ERROR : division by zero ***\n***Simpletron crashs the program ***\n";
				dumpMemory();
				return;
			}
			else
			{
				accumulator /= memory[operand];
				instructionCounter++;
			}
			break;
		}
		case 33://multiply
		{
			accumulator *= memory[operand];
			if (accumulator > 9999 || accumulator < -9999)//checking error
			{
				debug << "*** ERROR : overflow accumulator ***\n***Simpletron crashs the program ***\n";
				dumpMemory();
				return;
			}
			instructionCounter++;
			break;
		}
		case 40://branch
		{
			instructionCounter = operand;
			break;
		}
		case 41://branch if register < 0
		{
			if (accumulator < 0)
			{
				instructionCounter = operand;
			}
			else
			{
				instructionCounter++;
			}
			break;
		}
		case 42://branch if register = 0
		{
			if (accumulator == 0)
			{
				instructionCounter = operand;
			}
			else
			{
				instructionCounter++;
			}
			break;
		}
		case 43://halt
		{
			debug << "*** Simpletron complete calculations ***\n";
			dumpMemory();
			return;
		}
		default://if incorrect operation code
		{
			debug << "*** ERROR : incorrect operation code ***\n***Simpletron crashs the program ***\n";
			dumpMemory();
			return;
		}
		}
	}
	return;
}
void SimpletronEmulator::dumpMemory()
{
	debug << "REGISTERS :\n";//print registers
	debug << left << setw(22) << setfill(' ') << "accumulator" << internal << showpos << setfill('0') << setw(5) << accumulator << "\n";
	debug << left << setw(25) << setfill(' ') << "instructionCounter" << right << noshowpos << setfill('0') << setw(2) << instructionCounter << "\n";
	debug << left << setw(22) << setfill(' ') << "instructionRegister" << internal << showpos << setfill('0') << setw(5) << instructionRegister << "\n";
	debug << left << setw(25) << setfill(' ') << "operationCode" << right << noshowpos << setfill('0') << setw(2) << operationCode << "\n";
	debug << left << setw(25) << setfill(' ') << "operand" << right << setfill('0') << setw(2) << operand << "\n\n";
	debug << "MEMORY :\n  ";//print memory
	for (int i = 0; i < 10; ++i)
		debug << setfill(' ') << setw(6) << i;
	for (int i = 0; i < 100; i++)
	{
		if (i % 10 == 0)
			debug << "\n" << left << noshowpos << setfill('0') << setw(2) << i;
		debug << " " << internal << showpos << setw(5) << memory[i];
	}
}