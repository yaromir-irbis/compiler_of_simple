project(simple_compiler)

set(SOURCE_LIB SimpleCompiler.cpp SimpletronEmulator.cpp)
set(SOURCE_EXE main.cpp)

add_library(simple_compiler STATIC ${SOURCE_LIB})

add_executable(program ${SOURCE_EXE})

target_link_libraries(program simple_compiler)
