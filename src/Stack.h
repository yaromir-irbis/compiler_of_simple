#ifndef STACK_H
#define STACK_H
#include <iostream>
using std::cout;
using std::cerr;
using std::ostream;

#include <cstdlib>
using std::exit;

#include "StackNode.h"

template <typename STACKTYPE>
class Stack {
public:
	Stack();
	~Stack();
	void push(const STACKTYPE&);
	bool pop();
	STACKTYPE top() const;
	bool isEmpty() const;
	void printStack(ostream& = cout) const;
private:
	StackNode<STACKTYPE>* topPtr;
};

template <typename STACKTYPE>
Stack<STACKTYPE>::Stack() :topPtr(0)
{
}
template <typename STACKTYPE>
Stack<STACKTYPE>::~Stack()
{
	StackNode<STACKTYPE>* curPtr = topPtr;//pointer for deleting nodes
	while (topPtr != 0)
	{
		topPtr = topPtr->nextPtr;//go to the next node
		delete curPtr;
		curPtr = topPtr;
	}	
}
template<typename STACKTYPE>
void Stack<STACKTYPE>::push(const STACKTYPE& v)
{
	StackNode<STACKTYPE>* curPtr = new StackNode<STACKTYPE>(v);
	if (topPtr != 0)
		curPtr->nextPtr = topPtr;//link the new node to the top of the stack 
	topPtr = curPtr;
}
//return true if operation is successful, return false if operation doesn't successful
template<typename STACKTYPE>
bool Stack<STACKTYPE>::pop()
{
	if (isEmpty())
		return false;
	else
	{
		StackNode<STACKTYPE>* tmpPtr = topPtr;
		topPtr = topPtr->nextPtr;//go to the next node
		delete tmpPtr;
		return true;
	}
}
template<typename STACKTYPE>
STACKTYPE Stack<STACKTYPE>::top() const
{
	if (!isEmpty())
		return topPtr->getValue();
	else
	{
		cerr << "Error : stack is empty!\n";
		exit(0);
	}
}
template<typename STACKTYPE>
bool Stack<STACKTYPE>::isEmpty() const
{
	return topPtr == 0;
}
template<typename STACKTYPE>
void Stack<STACKTYPE>::printStack(ostream& output) const
{
	StackNode<STACKTYPE>* curPtr = topPtr;
	while (curPtr != 0)
	{
		output << curPtr->getValue() << " ";
		curPtr = curPtr->nextPtr;//go to the next node
	}
}
#endif // !STACK_H

